import regex
import math

def gameToObject(game: str):
    values = game.split(';') # 1 blue, 1 green, 3 red
    reg = regex.compile(r'(\d+)\s+(\w+)')
    color_sums = {}
    for value in values:
        for match in reg.findall(value):
            number, color = match
            number = int(number)
            color_sums[color] = number if number > color_sums.get(color, 0) else color_sums.get(color, 0)
    return math.prod(color_sums.values())

with open("./2/input.txt") as file:
    values = [gameToObject(i.split(":")[1]) for i in file.read().splitlines()]
    f = sum([i for i in values])
    print(f)