import regex

def gameToObject(game: str):
    values = game.split(';') # 1 blue, 1 green, 3 red
    reg = regex.compile(r'(\d+)\s+(\w+)')
    color_sums = {}
    game_true = True
    for value in values:
        for match in reg.findall(value):
            number, color = match
            number = int(number)
            color_sums[color] = color_sums.get(color, 0) + number
        if int(color_sums.get('red', 0)) > 12 or int(color_sums.get('green', 0)) > 13 or int(color_sums.get('blue', 0)) > 14:
            game_true = False
        color_sums = {}
    return { 'valid': game_true }

with open("./2/input.txt") as file:
    values = [gameToObject(i.split(":")[1]) for i in file.read().splitlines()]
    f = [i for i, r in enumerate(values) if r['valid'] == True]
    print(sum(f) + len(f))