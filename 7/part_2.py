import math

# Ordering cards by "value"
with open("./7/input.txt") as file:
    example = [d for d in file.read().splitlines()]

power = {"A": 24, "K": 23, "Q": 22, "J": 11, "T": 20, "9": 19, "8": 18, "7": 17, "6": 16, "5": 15, "4": 14, "3": 13, "2": 12 }

def handPower(hand: str):
    return "".join([str(power[h]) for h in hand])

hands_with_bid = [r.split(" ") for r in example]

tableau_de_ca_2 = { 5: [], 4: [], 3_2: [], 3: [], 2_2: [], 2: [], 1: [] }
for throw in hands_with_bid:
    # ['32T3K', '765']
    elem = { 5: 0, 4: 0, 3: 0, 2: 0, 1: 0 }
    checked_elements = []
    # Compter le nombre de cartes de même valeur
    number_of_j = 0
    for i in throw[0]:
        if i in checked_elements:
            continue
        checked_elements.append(i)
        values = throw[0].count(i)
        if i == "J" and throw[0].count(i) != 5:
            number_of_j = values
        else:
            elem[values] += 1
    higher_tuple = sorted(elem.items(), key=lambda x: x[0] if x[1] > 0 else 0)[-1]
    elem[higher_tuple[0]] -= 1
    elem[higher_tuple[0]+number_of_j] += 1
    throw.append(handPower(throw[0]))
    integer_value = int("".join([str(i) for i in elem.values()]))
    # 5 de même valeur
    if integer_value == 10000:
        tableau_de_ca_2[5].append(throw)
    # 4 de même valeur
    elif integer_value >= 1000:
        tableau_de_ca_2[4].append(throw)
    # 3 de même valeur et une paire
    elif integer_value >= 110:
        tableau_de_ca_2[3_2].append(throw)
    # 3 de même valeur
    elif integer_value >= 100:
        tableau_de_ca_2[3].append(throw)
    # 2 paires
    elif integer_value >= 20:
        tableau_de_ca_2[2_2].append(throw)
    # 1 paire
    elif integer_value >= 10:
        tableau_de_ca_2[2].append(throw)
    # rien
    else:
        tableau_de_ca_2[1].append(throw)

sorted_index = [index for index in reversed(tableau_de_ca_2.keys())]

multiplier = 0
bonjour = []
for index in sorted_index:
    sorted_hands_with_same_power = sorted(tableau_de_ca_2[index], key=lambda x: int(x[2]))
    for set_of_cards in sorted_hands_with_same_power:
        multiplier += 1
        bonjour.append(int(set_of_cards[1]) * multiplier)

print(len(example), multiplier)
print(sum(bonjour))

