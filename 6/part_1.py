import math

with open("./6/input.txt") as file:
    test = [p for p in [s.split(" ") for s in file.read().splitlines()]]
    values = []
    for s in test:
        f = []
        for i, j in enumerate(s):
            if j == '' or j.find(':') != -1:
                continue
            f.append(j)
        values.append(f)
    azd = list(zip(values[0], values[1]))

rr = [] 
for t in azd:
    time = int(t[0])
    distance = int(t[1])
    ways_to_win = []
    for hold in range(0, time):
        dist = hold * (time - hold)
        if dist >= distance:
            ways_to_win.append(time)
    rr.append(len(ways_to_win))

print(math.prod(rr))

