import math

with open("./6/input.txt") as file:
    test = [p for p in [s.split(" ") for s in file.read().splitlines()]]
    values = []
    for s in test:
        f = ""
        for i, j in enumerate(s):
            if j == '' or j.find(':') != -1:
                continue
            f += j
        values.append(f)

print(values)
time = int(values[0])
distance = int(values[1])
ways_to_win = []
for hold in range(0, time):
    dist = hold * (time - hold)
    if dist >= distance:
        ways_to_win.append(time)

print(len(ways_to_win))

