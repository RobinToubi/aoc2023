with open("./3/input.txt") as file:
    example = [list(i) for i in file.read().splitlines()]

symbol_type = ['0','1','2','3','4','5', '6', '7', '8', '9', '.']

# 467..114..
# ...*......
# ..35..633.
# ......#...
# 617*......
# .....+.58.
# ..592.....
# ......755.
# ...$.*....
# .664.598..

# ligne
valid_numbers = []
for indexLine, line in enumerate(example):
    # on itère sur chaque caractère d'une ligne
    number = ""
    valid = False
    for indexChar, element in enumerate(line):
        # Si c'est un digit, on check si a droite & a gauche on a un symbole
        if element.isdigit():
            up = example[indexLine - 1][indexChar] not in symbol_type if indexLine > 0 else False
            down = example[indexLine + 1][indexChar] not in symbol_type if indexLine < len(example) - 1 else False
            left = example[indexLine][indexChar - 1] not in symbol_type if indexChar > 0 else False
            right = example[indexLine][indexChar + 1] not in symbol_type if indexChar < len(line) - 1 else False
            up_left = example[indexLine - 1][indexChar - 1] not in symbol_type if indexLine > 0 and indexChar > 0 else False
            up_right = example[indexLine - 1][indexChar + 1] not in symbol_type if indexLine > 0 and indexChar < len(line) - 1 else False
            down_left = example[indexLine + 1][indexChar - 1] not in symbol_type if indexLine < len(example) - 1 and indexChar > 0 else False
            down_right = example[indexLine + 1][indexChar + 1] not in symbol_type if indexLine < len(example) - 1 and indexChar < len(line) - 1 else False
            adjacent = up_left or up_right or down_left or down_right
            if (up or down or left or right or adjacent):
                valid = True
            if line[indexChar].isdigit():
                number = number + line[indexChar]
        elif valid == True or indexChar == len(line) - 1:
            valid_numbers.append(number)
            number = ""
            valid = False


# Gérer le cas de la fin de ligne
print(sum([int(s) for s in valid_numbers]))