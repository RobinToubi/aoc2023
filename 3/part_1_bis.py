import requests

YEAR = 2023
AOC_COOKIE = "53616c7465645f5f5eba4c6a45e12f89b7ce80820b5b041d8dde0cc688514e60cf5aed0831d73ef55c5889b76b103604de9a0970f69c7c8d6d49f0b64f4d8256"

#def get_example(day,offset=0):
#    req = requests.get(f'https://adventofcode.com/{YEAR}/day/{day}', headers={'cookie':'session='+AOC_COOKIE})
#    return req.text.split('<pre><code>')[offset+1].split('</code></pre>')[0]
#
#example = [list(i) for i in get_example(3,offset=0).splitlines()]

# 467..114..
# ...*......
# ..35..633.
# ......#...
# 617*......
# .....+.58.
# ..592.....
# ......755.
# ...$.*....
# .664.598..

with open("./3/input.txt") as file:
    example = [list(i) for i in file.read().splitlines()]
    print(example)

# symboles classiques
normal_symbol_type = ['0','1','2','3','4','5', '6', '7', '8', '9', '.']

for index, element in enumerate(example):
    # ['4', '6', '7', '.', '.', '1', '1', '4', '.', '.']
    # ['.', '.', '.', '*', '.', '.', '.', '.', '.', '.']
    number_indexes = [index for index, char in enumerate(element) if char.isdigit()]
    # Tester si on a un symbole autour des nombres créés à partir des index qui se suivent
    b = [j for i, j in range(index, len(example) - 1) if example[i + 1] in number_indexes and j not in normal_symbol_type]
    print(b)