def parse(s: str):
    a = [[int(s) for s in d if s != ''] for d in [res.split(' ') for res in [w_r for w_r in s.split('|')]]]
    returned = len([i for i in a[1] if i in a[0]])
    return 2**(returned-1) if returned > 0 else 0

with open("./4/input.txt") as file:
    print(sum([parse(i.split(':')[1]) for i in file.read().splitlines()]))