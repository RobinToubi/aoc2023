def firstAndLastDigit(txt: str):
    v = [s for s in list(txt) if s.isdigit()]
    return int(v[0] + v[-1])

with open("./1/input.txt") as file:
    values = sum([firstAndLastDigit(i) for i in file.read().splitlines()])
    print(values)