import regex
# not finished
table = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}

def get(s: str):
    reg = r"[0-9]|one|two|three|four|five|six|seven|eight|nine"
    test = regex.compile(reg).findall(s)
    a = test[0] if str(test[0]).isdigit() else table[test[0]]
    b = test[-1] if str(test[-1]).isdigit() else table[test[-1]]
    return a+b

print(get("nineight"))

with open("./1/input.txt") as file:
    values = sum([int(get(i)) for i in file.read().splitlines()])
    print(values) 